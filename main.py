# Main Script for Mechatronics Final Project Robot
#
#
#
#
# Updated: 4/19/16

# Custom Modules
import SensorHandler
import ImageHandler
import SearchHandler
import MotorHandler

sensors = SensorHandler.SensorHandler()
im_handler = ImageHandler.ImageHandler()
search = SearchHandler.SearchHandler()
motors = MotorHandler.MotorHandler()

# OpenCV, etc
import time
import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import RPi.GPIO as GPIO

# Camera parameters
res_h = 480
res_w = 640
fr = 90

cam = PiCamera()
cam.resolution = (res_w, res_h)
cam.framerate = fr
rgb_array = PiRGBArray(cam)
time.sleep(.2)
start_time = time.time()
total_frames = 0

# Prime filters
for i in range(5):
    sensors.read()

try:
    for frame in cam.capture_continuous(rgb_array, format="bgr", use_video_port=True):
        # Capture image
        im = frame.array
        rgb_array.truncate(0)

        # Read and filter raw data from sensors
        sensors.read()
    
        # Process image        
        im_handler.processImage(im)

        # Update SearchHandler with sensor data, image info, delta pos
        d = sensors.getDistances()
        print d
        tree_status = im_handler.getTreeStatus()
        gift_status = im_handler.getGiftStatus()
        print 'gift status : ' + str(gift_status)
        o_error = im_handler.getOrientationError()
        search.update(0, o_error, d, gift_status, tree_status)
        
        # Update MotorHandler with most recent commands from search module
        ms = search.getMotorState()
        print 'Motors: ' + str(ms)
        motors.update(ms[0], ms[1], ms[2])

        # Check for actions
        a = search.getAction()
        if a[0] and a[1]:
            print 'Done!'
        elif a[0]:
            print 'Pickup'
            time.sleep(2)
            motors.pickUp()
            time.sleep(2)
        elif a[1]:
            print 'Dropping'
            time.sleep(2)
            motors.handOpen()
            time.sleep(2)                

        total_frames += 1

        # if total_frames % 5 == 0:
        #     cv2.imwrite('test_ims_DONOTCOMMIT/test_im_%d.jpg' % total_frames, im)

        
        cv2.waitKey(1)
        
#        if total_frames % 100 == 0:
 #           print 'Current average fps: ' + str(total_frames / (time.time() - start_time))

except KeyboardInterrupt:
    # Clean up
    cam.close()
    print 'Total average fps: ' + str(total_frames / (time.time() - start_time))
    GPIO.cleanup()
