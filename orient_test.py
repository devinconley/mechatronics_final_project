# Test script for extracting orientation from peg pattern
import numpy as np
import cv2
import math

# load test image...
im = cv2.imread('test_ims_DONOTCOMMIT/test_im_910.jpg')
# pre process
im = cv2.flip(im, 0)
im = cv2.flip(im, 1)
im = cv2.blur(im, (5,5))
im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

# Crop and threshold to binary
h, w = im_gray.shape
sz = 120
im_roi = im_gray[h-sz:h, (w-sz)/2:(w+sz)/2]
ret, im_bin = cv2.threshold(im_roi, 127, 255, cv2.THRESH_BINARY_INV)

# Get contours
contours = cv2.findContours(im_bin.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

# Store centers
centers = []
for c in contours:
    x, y, w, h = cv2.boundingRect(c)
    centers.append((x+w/2, y+h/2))

# Draw connections
im_roi_bgr = cv2.cvtColor(im_roi, cv2.COLOR_GRAY2BGR)
dh = 0
dw = 0
used_edges = []
max_diff = sz/4
print centers
for i in range(len(centers)):
    best_val = max_diff
    best_index = 0
    for j in range(len(centers)):
        temp_val = abs(centers[i][0] - centers[j][0])
        if temp_val < best_val and i != j:
            best_index = j
            best_val = temp_val
    if ((i, best_index) not in used_edges) and ((best_index,i) not in used_edges) and best_val != max_diff:
        used_edges.append((i,best_index))
        temp = centers[i][0] < centers[best_index][0]
        dh += (centers[i][1] - centers[best_index][1])*pow(-1, temp+1)
        dw += (centers[best_index][0] - centers[i][0])*pow(-1, temp+1)
        cv2.line(im_roi_bgr, centers[i], centers[best_index], (0, 255, 0))

print dh
print dw
angle_error = 90 - (180.0/math.pi)*math.atan2(dh,dw)
#cv2.circle(im_roi_bgr, c, 5, (0,255,0))    
print angle_error

cv2.imshow('window', im_roi_bgr);
cv2.waitKey(0)
