# Class for handling motor control
#
#
#

import RPi.GPIO as GPIO
import time

# Pin Declarations
# Pin rules:

MTR_PINS = [21, 25, 6, 13, 18, 4]
#     fr_a, fr_b, fl_a, fr_b, b_a, b_b
SRVO_PINS = [24, 23]
#           hand, arm

# Other Constants
ERR_THRESH = 0.04 # (rad)
PWM_HZ = 50 # (hz)
SPEED = 25 # (percent duty cycle)
ROT_SPEED = 50
STEERING_COEFF = 80
TIME_STEP = 0.05 # Seconds
TURN_TIME_APPROX = 0.55


class MotorHandler:
    def __init__(self):
        
        # Pin setup
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.pwm = []
        for i in MTR_PINS:
            GPIO.setup(i, GPIO.OUT)
            temp = GPIO.PWM(i, PWM_HZ)
            temp.start(0)
            self.pwm.append(temp)

        #self.pwm[4].ChangeDutyCycle(0)
        #self.pwm[5].ChangeDutyCycle(0)
        """
        for i in range(4):
            GPIO.setup(MTR_PINS[i], GPIO.OUT)
            temp = GPIO.PWM(MTR_PINS[i], PWM_HZ)
            temp.start(0)
            self.pwm.append(temp)
        """
        for j in SRVO_PINS:
            GPIO.setup(j, GPIO.OUT)
            ntemp = GPIO.PWM(j, PWM_HZ)
            ntemp.start(0)
            self.pwm.append(ntemp)
        
        # Raise the arm
        self.pwm[7].ChangeDutyCycle(9)
        time.sleep(1)
        # Open the claw
        self.pwm[6].ChangeDutyCycle(12)
        time.sleep(1)
        # Other setup
        self.desired_orientation = 0
        self.move_forward = False
        self.last_adjust = 0
        
    def update(self, move_forward, delta_orientation, speed_modifier):
        
        # Correct error by steering
        if move_forward:
            # Only need to change front wheels if was False last frame
            if time.time() - self.last_adjust > TIME_STEP:  
                #print "move forward with speed mod: " + str(speed_modifier)
                if speed_modifier > 0:
                    self.pwm[0].ChangeDutyCycle(speed_modifier*SPEED)
                    self.pwm[1].ChangeDutyCycle(0)
                    self.pwm[2].ChangeDutyCycle(0)
                    self.pwm[3].ChangeDutyCycle(speed_modifier*SPEED)
                    self.move_forward = move_forward
                else:
                    self.pwm[0].ChangeDutyCycle(0)
                    self.pwm[1].ChangeDutyCycle(abs(speed_modifier)*SPEED)
                    self.pwm[2].ChangeDutyCycle(abs(speed_modifier)*SPEED)
                    self.pwm[3].ChangeDutyCycle(0)
                    
            # Correct orientation by back wheel via gradient descent
            # Only adjust if enough time has passed
            
            if time.time() - self.last_adjust > TIME_STEP:
                steer = STEERING_COEFF*abs(delta_orientation)/180.0
                #print steer
                #print error
                GPIO.setup(MTR_PINS[4], GPIO.OUT)
                GPIO.setup(MTR_PINS[5], GPIO.OUT)
                self.pwm[4].ChangeDutyCycle(steer*(delta_orientation > 0))
                #GPIO.output(MTR_PINS[4], GPIO.HIGH)
                #GPIO.output(MTR_PINS[5], GPIO.LOW)
                #print (steer*(error < 0))
                self.pwm[5].ChangeDutyCycle(steer*(delta_orientation < 0))
                #print (steer*(error > 0))

                self.last_adjust = time.time()
                self.move_forward = move_forward
                        
        # Make turn (+/-90 or 180)
        else:
            t = 0
            if delta_orientation == 90 or delta_orientation == -90:
                t = TURN_TIME_APPROX
            elif delta_orientation == -180:
                t = 2*TURN_TIME_APPROX
            else:
                t = TURN_TIME_APPROX * abs(delta_orientation/180.0)
            cw = delta_orientation > 0
            print 'Turning: ' + cw*'cw: ' + (not cw)*'ccw: ' + str(delta_orientation)
            # Make explicit turn
            for i in range(6):
                rot_speed = ROT_SPEED*( (i+cw+1) % 2 == 0)
                #print str(i) + ': ' + str(rot_speed)
                GPIO.setup(MTR_PINS[i], GPIO.OUT)
                self.pwm[i].ChangeDutyCycle(rot_speed)
            # Estimate turn time
            time.sleep(t)
            # Turn of motors
            for i in range(6):
                rot_speed = 0
                GPIO.setup(MTR_PINS[i], GPIO.OUT)
                self.pwm[i].ChangeDutyCycle(rot_speed)
        
            """
            if abs(error) > ERR_THRESH:
                if time.time() - self.last_adjust > TIME_STEP:
            cw = 1*(error < 0)
                    print 'turning: ' + 'cw'*(cw) + 'ccw'*(not cw)
                    for i in range(6):
                        rot_speed = 99.00*abs(error/180.0)*( (i+cw) % 2 == 0)
                        #print str(i) + ': ' + str(rot_speed)
                        GPIO.setup(MTR_PINS[i], GPIO.OUT)
                        self.pwm[i].ChangeDutyCycle(rot_speed)
                    # Set to turn for approx number of seconds. Adjust after
                    self.last_adjust = time.time() + TURN_TIME_APPROX
                    self.move_forward = move_forward
            """
                    
    # Will return actual difference of angles to account for circle 
    def __angleDiff(self, angle_a, angle_b):
        temp = angle_a - angle_b
        while temp > 180 or temp < -180:
            if temp > 180:
                temp -= 360
            elif temp < -180:
                temp += 360
        return temp

    def __del__(self):
        print 'Destroying...'
        for i in self.pwm:
            i.ChangeDutyCycle(0)
            i.stop()
    
    def pickUp(self):
        print 'Pickup called'
        # Lower the arm
        self.pwm[7].ChangeDutyCycle(3)
        time.sleep(1)
        # Close the hand
        self.pwm[6].ChangeDutyCycle(4)
        time.sleep(1)
        # Raise the arm while opening hand
        for i in range(3, 10):
            self.pwm[7].ChangeDutyCycle(i)
            temp = 4 + 2*(i/9)
            self.pwm[6].ChangeDutyCycle(temp)
            time.sleep(.1)

    def drop(self):
        print 'Drop called'
        # Partially lower the arm
        self.pwm[7].ChangeDutyCycle(5)
        time.sleep(1)
        # Open the claw
        self.pwm[6].ChangeDutyCycle(12)
        time.sleep(1)

    def __handClose(self):
        self.pwm[6].ChangeDutyCycle(6)

    def handOpen(self):
        self.pwm[7].ChangeDutyCycle(9)
        time.sleep(1.5)
        self.pwm[6].ChangeDutyCycle(12)
        self.pwm[7].ChangeDutyCycle(7.5)
        time.sleep(2)
        self.update(True, 0, -1)
        time.sleep(2)
        self.update(False, 0, 0)

    def __armUp(self):
        # Be careful testing this! Can sometimes move all the way up
        # Rotate up for 2 seconds
        self.pwm[7].ChangeDutyCycle(6)
        time.sleep(2)
        # Set to stationary PWM
        self.pwm[7].ChangeDutyCycle(7.5)

    def __armDown(self):
        # Be careful testing this!
        # Rotate down for 2 seconds
        self.pwm[7].ChangeDutyCycle(9)
        time.sleep(2)
        # Set tp stationary PWM
        self.pwm[7].ChangeDutyCycle(7.5)
