# Class for handling Maze Search Algorithm
#
# Currently written for an individual, but will be converted to a partner search
# Using stack for a depth first search

import numpy as np
import time

WALL_THRESH = 10 # min distance to be considered open
FRONT_WALL = 3
GRID_RES = 1 # (units of peg holes)
dir = ['r', 'u', 'l', 'd']
DELTA_POS = [(1, 0), (0, 1), (-1, 0), (0, -1)]
TIME_AFTER_TURN = 3.0
AIM_PIXEL_X = 485
AIM_PIXEL_Y = 280
NUM_FRAMES_OPEN = 2
AIM_TURN_COEFF = .06
AIM_MOVE_STEP = 0.4
AIM_THRESH = 40
IN_POS_THRESH = 15

class SearchHandler:
    def __init__(self):
        """
        self.origin = (0, 0)
        self.stack = []
        self.temp_traveled = 0
        self.pos_curr = (self.origin)
        self.pos_next = (self.origin)
        self.dir_curr = 1 # assume facing up to start
        """
        self.turned_right = False
        self.time_turn_right = 0
        self.right_open_counter = 0
        self.left_open_counter = 0
        self.u_turn_counter = 0
        self.in_position_counter = 0
        self.tree_seen = False
        self.gift_found = False
        self.in_position = False
        self.done = False
        
    def update(self, dist_forward, orientation_error, dist_sensors, gift_status, tree_status):
        #self.temp_traveled += dist_forward
        self.orientation_error = orientation_error
        self.dist_sensors = dist_sensors
        self.gift_status = gift_status
        self.tree_status = tree_status

        # Update counters for consecutive frames with open paths (want to be centered)
        # Right
        if dist_sensors[0] > WALL_THRESH:
            self.right_open_counter += 1
        else:
            self.right_open_counter = 0
        # Left
        if dist_sensors[1] > WALL_THRESH:
            self.left_open_counter +=1
        else:
            self.left_open_counter = 0
        # U-turn
        if dist_sensors[0] < WALL_THRESH and dist_sensors[1] < WALL_THRESH and dist_sensors[2] < FRONT_WALL:
            self.u_turn_counter += 1
        else:
            self.u_turn_counter = 0
            
        # Re-allow right hand turns after alloted time
        if time.time() - self.time_turn_right > TIME_AFTER_TURN:
            self.turned_right = False            


        """
        if self.temp_traveled >= GRID_RES:
            # Update current position
            self.pos_curr += DELTA_POS[self.dir_curr]

            # Check if reached next node to be expanded
            if self.pos_curr ==  self.pos_next:
                self.__explore()
        """

    # Currently right hand wall follower with priority for items in view
    def getMotorState(self):
        # Completion
        if self.done:
            return (False, 0, 0)
        # Gift logic
        elif self.gift_status[0] and not self.gift_found:
            print 'see gift'
            self.right_open_counter = 0
            self.left_open_counter = 0
            self.u_turn_counter = 0
            #print self.gift_status
            # x-alignment
            #temp_aim_pixel_x = 320 + ((AIM_PIXEL_X - 320) / AIM_PIXEL_Y)*self.gift_status[2]
            temp_aim_pixel_x = AIM_PIXEL_X
            turn = AIM_TURN_COEFF*(self.gift_status[1] - temp_aim_pixel_x)
            aligned_x = abs(self.gift_status[1] - temp_aim_pixel_x) < IN_POS_THRESH
            turn = turn*(not aligned_x)
            aimed_x = abs(self.gift_status[1] - temp_aim_pixel_x) < AIM_THRESH
            
            # y-alignment
            speed_mod = AIM_MOVE_STEP*pow(-1, 1 + (AIM_PIXEL_Y > self.gift_status[2]))
            aligned_y = abs(self.gift_status[2] - AIM_PIXEL_Y) < IN_POS_THRESH
            speed_mod = speed_mod*(not aligned_y)
            
            # Check for being in position
            if aligned_x and aligned_y:
                self.in_position_counter += 1
                if self.in_position_counter > NUM_FRAMES_OPEN:
                    self.in_position = True
                return (False, turn, 0)
            else:
                self.in_position_counter = 0
                return (aimed_x, turn, speed_mod)
        # Tree logic
        elif self.tree_status[0] and self.gift_found:
            print 'seeing tree?'
            # Align at offset by linear equation
            temp_aim_pixel_x = 320 + ((AIM_PIXEL_X - 320) / AIM_PIXEL_Y)*self.tree_status[2]
            turn = AIM_TURN_COEFF*(self.tree_status[1] - temp_aim_pixel_x)
            aligned = abs(self.tree_status[1] - temp_aim_pixel_x) < AIM_THRESH
            if self.tree_status[2] > AIM_PIXEL_Y and aligned:
                self.in_position = True
                return (False, turn, 0)
            else:
                return (aligned, turn, AIM_MOVE_STEP)
        # Right turn Logic
        elif self.right_open_counter > NUM_FRAMES_OPEN and not self.turned_right:
            self.turned_right = True
            self.time_turn_right = time.time()
            return (False, 90, 1)
        # Forward logic
        elif self.dist_sensors[2] > FRONT_WALL:
            return (True, -self.orientation_error, 1)
        # Left turn logic
        elif self.left_open_counter > NUM_FRAMES_OPEN:
            return (False, -90, 0)
        # U turn logic
        elif self.u_turn_counter > NUM_FRAMES_OPEN:
            return (False, -180, 0)
        # Stall logic...
        else:
            return (False, 0, 0)


    def getAction(self):
        # Done?
        if self.done:
            return (True, True)
        # Triggering Pickup in main        
        elif self.in_position and not self.gift_found:
            self.gift_found = True
            self.in_position = False
            return (True, False)
        # Triggering drop in main
        elif self.in_position:
            self.done = True
            self.in_position = False
            return (False, True)
        else:
            return (False, False)
        
    """    
    def __explore(self, dist_sensors):
        # Assume that we entered this position straight on and have not turned.
        for i in range(0,3):
            # Check that a path exists
            if dist_sensors[i] > WALL_THRESH:
                # Convert to absolute direction
                dir_abs = i - (1 - self.dir_curr)
                if dir_abs < 0:
                    dir_abs += 4
                elif dir_abs > 3:
                    dir_abs -= 4    

                # Push this to stack for later exploration
                self.stack.append(self.pos_curr + DELTA_POS[dir_abs])

        # Pop new node from stack for next exploration
        self.pos_next = self.stack.pop()
    """
                    
                    

        
    # In circular scan, can also use camera to mark entire paths as explored
    # (Looking for complete purple contour)
