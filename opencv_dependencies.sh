# Run this shell script before building OpenCV

sudo apt-get update
sudo apt-get upgrade

# For build
sudo apt-get install build-essential pkg-config cmake cmake-curses-gui python2.7-dev

# Image support
sudo apt-get install libpng12-0 libpng12-dev libpng++-dev libpng3 libpnglite-dev zlib1g-dbg zlib1g zlib1g-dev pngtools libtiff5-dev libtiff5 libtiffxx0c2 libtiff-tools libeigen3-dev

# Other
sudo apt-get install libjpeg8 libjpeg8-dev libjpeg8-dbg libjpeg-progs libavcodec-dev libavformat-dev libxine2-ffmpeg libxine2-dev libxine2-bin libunicap2 libunicap2-dev swig libv4l-0 libv4l-dev python-numpy libgtk2.0-dev
