import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
signal = 18
GPIO.setwarnings(False)

timeout = 0.020
distance = 0
avg = 0
counter = 0

while counter < 5:
	GPIO.setup(signal, GPIO.OUT)
	#clean up the output
	GPIO.output(signal, 0)
	
	#timing for ping
	time.sleep(0.000002)

	#send a signal
	GPIO.output(signal, 1)
	time.sleep(0.000005)

	GPIO.output(signal, 0)
	GPIO.setup(signal, GPIO.IN)

	reading = True
	begin = time.time()
	while GPIO.input(signal) == 0 and reading:
		stime = time.time()
		if (stime - begin > timeout):
			reading = False
	
	if reading:
		begin = time.time()
		while GPIO.input(signal) == 1 and reading:
			etime = time.time()
			if (etime - begin > timeout):
				raeding = False

	if reading:
		duration = etime - stime
		distance = duration*34000/2
		avg += distance
		#print distance

	counter += 1

avg = avg/5
print avg
