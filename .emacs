
(setq backup-directory-alist '(("." . "~/.backups")))
(custom-set-variables
 )
(custom-set-faces
 '(custom-group-tag ((t (:inherit variable-pitch :foreground "brightcyan" :weight bold :height 1.2))))
 '(custom-state ((t (:foreground "brightyellow"))))
 '(custom-variable-tag ((t (:foreground "brightmagenta" :weight bold))))
 '(font-lock-builtin-face ((t (:foreground "brightgreen"))))
 '(font-lock-comment-face ((t (:foreground "brightyellow"))))
 '(font-lock-function-name-face ((t (:foreground "color-63"))))
 '(font-lock-keyword-face ((t (:foreground "brightcyan"))))
 '(font-lock-string-face ((t (:foreground "color-178"))))
 '(font-lock-type-face ((t (:foreground "color-47"))))
 '(font-lock-variable-name-face ((t (:foreground "color-141"))))
 '(link ((t (:foreground "brightblue" :underline t))))
 '(minibuffer-prompt ((t (:foreground "color-46")))))

