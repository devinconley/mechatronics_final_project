# Class for image handling

import numpy as np
import time
import cv2
import math

# Frame interval for showing images (should be high if over ssh)
IM_DISPLAY_INTERVAL = 10000

# Color ranges for Objects of Interst in HSV domain
LOWER_GREEN = np.array([40, 150, 10])
UPPER_GREEN = np.array([60, 256, 155])
BGR_GREEN = np.array([0, 255, 0])

LOWER_RED = np.array([0, 50, 50])
UPPER_RED = np.array([10, 255, 255])
BGR_RED = np.array([0, 0, 255])

LOWER_WALLS = np.array([15, 100, 80])
UPPER_WALLS = np.array([35, 140, 120])
BGR_WALLS = np.array([255, 0, 127])

# Size criteria for different object
MIN_AREA_GIFT = 4000 # green
MIN_AREA_TREE = 4000 # red
MIN_PATH_WIDTH = 100
MAX_PATH_NOISE = 50 #

# Constants for peg hole analysis
HOLES_ROI_SZ = 200
HOLES_MAX_DIFF = 40
HOLES_BW_THRESH = 50

class ImageHandler:
    def __init__(self):
        self.im_last = []
        self.im_curr = []
        self.counter = IM_DISPLAY_INTERVAL
        self.front_clear = False
        self.front_pos = 0
        self.right_clear = False
        self.right_pos = 0
        self.left_clear = False
        self.left_pos = 0
        self.gift_status = (False, 0, 0, 0, 0)
        self.tree_status = (False, 0, 0, 0, 0)
        self.orientation_error = 0
        
    def processImage(self, new_image):
        # Pre-processing
        new_image = cv2.flip(new_image, 0)
        new_image = cv2.flip(new_image, 1)
        new_image = cv2.blur(new_image, (5,5))
        
        self.im_last = self.im_curr
        self.im_curr = new_image
        
        # Convert to HSV and generate color masks
        im_hsv = cv2.cvtColor(new_image, cv2.COLOR_BGR2HSV)
        mask_green = cv2.inRange(im_hsv, LOWER_GREEN, UPPER_GREEN)
        mask_red = cv2.inRange(im_hsv, LOWER_RED, UPPER_RED)
        mask_walls = cv2.inRange(im_hsv.copy(), LOWER_WALLS, UPPER_WALLS)
        
        # Search for contours that might be object
        contours_green = cv2.findContours(mask_green.copy(), cv2.RETR_EXTERNAL,
                                          cv2.CHAIN_APPROX_SIMPLE)[-2]
        contours_red = cv2.findContours(mask_red.copy(), cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)[-2]

        # Will only look at largest contour (if any) - Green
        if len(contours_green) > 0:
            c = max(contours_green, key=cv2.contourArea)
            x, y, w, h = cv2.boundingRect(c)
            if w*h > MIN_AREA_TREE:
                #cv2.rectangle(new_image, (x, y), (x + w, y + h), BGR_GREEN, 1)
                self.tree_status = (True, x+w/2, y+h/2, w, h)
            else:
                self.tree_status = (False, 0, 0, 0, 0)
                
        # Look at largest red contour
        if len(contours_red) > 0:
            c = max(contours_red, key=cv2.contourArea)
            x, y, w, h = cv2.boundingRect(c)
            if w*h > MIN_AREA_GIFT:
                cv2.rectangle(new_image, (x, y), (x + w, y + h), BGR_RED, 1)
                self.gift_status = (True, x+w/2, y+h/2, w, h)
            else:
                self.gift_status = (False, 0, 0, 0, 0)
                
            #print self.gift_status
        # Peg hole analysis
        h, w, c = new_image.shape
        im_holes_roi = cv2.cvtColor(new_image[h-HOLES_ROI_SZ:h,(w-HOLES_ROI_SZ)/2:
                                              (w+HOLES_ROI_SZ)/2], cv2.COLOR_BGR2GRAY)
        ret, im_holes_bw = cv2.threshold(im_holes_roi, HOLES_BW_THRESH, 255,
                                         cv2.THRESH_BINARY_INV);
        
        # Get contours
        contours = cv2.findContours(im_holes_bw.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)[-2]

        # Store centers
        centers = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            centers.append((x+w/2, y+h/2))

        # Connect likely points and look at average slope
        im_holes_bgr = cv2.cvtColor(im_holes_roi, cv2.COLOR_GRAY2BGR)
        dh = 0
        dw = 0
        used_edges = []
        #print centers
        for i in range(len(centers)):
            best_val = HOLES_MAX_DIFF
            best_index = 0
            # Look for closest in x-direction
            for j in range(len(centers)):
                temp_val = abs(centers[i][0] - centers[j][0])
                if temp_val < best_val and i != j:
                    best_index = j
                    best_val = temp_val
            if ((i, best_index) not in used_edges) and ((best_index, i) not in used_edges) and best_val != HOLES_MAX_DIFF:
                used_edges.append((i,best_index))
                temp = centers[i][1] < centers[best_index][1]
                dh += abs(centers[i][1] - centers[best_index][1])
                dw += (centers[best_index][0] - centers[i][0])*pow(-1, temp+1)
                cv2.line(im_holes_bgr, centers[i], centers[best_index], (0, 255, 0))
                
        #print dh
        #print dw
        if len(used_edges) > 0:
            self.orientation_error = 90 - (180.0/math.pi)*math.atan2(dh,dw)
        else:
            self.orientation_error = 0
        #cv2.circle(im_roi_bgr, c, 5, (0,255,0))    
        #print self.orientation_error


        """
        # Check top half of image for open paths in front and to sides 
        # Front
        for i in range(0, w - MIN_PATH_WIDTH, MIN_PATH_WIDTH):
            # Look at bottom of image for a clear path to move forward
            roi = mask_walls[0.8*h:h-1, i:i+MIN_PATH_WIDTH]
            if cv2.mean(roi)[0] < MAX_PATH_NOISE:
                self.front_clear = True
                self.front_pos = i
                break
            self.front_clear = False

        
        # Right
        for i in range(h/2 - MIN_PATH_WIDTH):
            roi = mask_walls[i:i+MIN_PATH_WIDTH, (3/4)*w:w-1]
            if cv2.mean(roi)[0] < MAX_PATH_NOISE:
                self.right_clear = True
                self.right_pos = i
                break
            self.right_clear = False
            
        # Left
        for i in range(h/2 - MIN_PATH_WIDTH):
            roi = mask_walls[i:i+MIN_PATH_WIDTH, 0:w/4]
            if cv2.mean(roi)[0] < MAX_PATH_NOISE:
                self.left_clear = True
                self.left_pos = i
                break
            self.left_clear = False
        """
        
        if self.counter > IM_DISPLAY_INTERVAL:
            # Graphics
            color = self.front_clear*BGR_GREEN + (not self.front_clear)*BGR_WALLS
            cv2.line(new_image, (w/2-MIN_PATH_WIDTH/2,0), (w/2+MIN_PATH_WIDTH/2,0), color, 10)
            #color = self.right_clear*BGR_GREEN + (not self.right_clear)*BGR_WALLS
            #cv2.line(new_image, (w-1,h/2-MIN_PATH_WIDTH/2),(w-1,h/2+MIN_PATH_WIDTH/2),color, 10)
            #color = self.left_clear*BGR_GREEN + (not self.left_clear)*BGR_WALLS
            #cv2.line(new_image, (0,h/2-MIN_PATH_WIDTH/2),(0,h/2+MIN_PATH_WIDTH/2), color, 10)
            # Display
            cv2.imshow("Image", im_holes_bgr) 
            #cv2.imshow("Wall Mask", mask_walls)
            self.counter = 0

            # Check average value of a wall color
            #sample_hsv = im_hsv[40:90,400:450, :]
            #sample = new_image[40:90,400:450,:]
            #cv2.imshow("sample", sample)
            #print cv2.mean(sample_hsv)

            
        self.counter += 1

    def getDeltaPos(self):
        return (0,0)

    def getTreeStatus(self):
        return self.tree_status

    def getGiftStatus(self):
        return self.gift_status

    def getClearSides(self):
        return (self.right_clear, self.front_clear, self.left_clear)
    
    def getOrientationError(self):
        return self.orientation_error
