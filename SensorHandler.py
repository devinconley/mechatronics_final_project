# Class for handling and filtering raw sensor data

# Import any necessary modules for reading distance sensors
import RPi.GPIO as GPIO
import time
import Adafruit_LSM303
import math
import numpy as np

# IIR filter coefficients
DIST_PINS = [17, 12, 5] # Right, Left, Front
ORDER = 3
#DIST_FF = [1, -0.5772, 0.4218, -0.0563] # Will implement filters at some point
#DIST_FB = [.0985, 0.2956, 0.2956, .0985]
DIST_FF = [0.25, 0.25, 0.25, 0.25]
DIST_FB = [0, 0, 0, 0]
MAG_FF = [1, 0, 0]
MAG_FB = [0, 0, 0]
MAG_MIN_X = -57 #-70;
MAG_MAX_X = 273 #401;
MAG_MIN_Y = -254 #-364;
MAG_MAX_Y = 94 #133;

class SensorHandler:
    def __init__(self):
        # Setup data storage
        self.dist_raw = [[0 for j in range(ORDER+1)] for i in range(len(DIST_PINS))]
        self.mag_raw = [0 for i in range(ORDER+1)]
        self.dist_filt = [[0 for j in range(ORDER+1)] for i in range(len(DIST_PINS))]
        self.mag_filt = [0 for i in range(ORDER+1)]
        self.mov_filt = [[0 for j in range(ORDER+1)] for i in range(len(DIST_PINS))]
        # Pin setup        
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        # Instance of LSM303 and magnetometer setup
        #self.lsm303 = Adafruit_LSM303.LSM303()
        self.offset_x = (MAG_MIN_X + MAG_MAX_X)/2;
        self.offset_y = (MAG_MIN_Y + MAG_MAX_Y)/2;

        
    def read(self, num = 1):
        # Default behaviour will be to only read and filter one value for each sensor
        for i in range(num):
            # Read in raw value for PING sensor
            for j in range(len(DIST_PINS)):
                self.dist_raw[j].pop() #dump oldest raw val from tail
                read_dist = self.__readPing(DIST_PINS[j]) #read in a value from the Ping sensor
                self.dist_raw[j].insert(0, read_dist)
                #print read_dist
            """
            # Read in new raw val for magnetometer
            self.mag_raw.pop()
            read_mag = self.__getOrientationLSM303()
            self.mag_raw.insert(0, read_mag)
            """
            # Filter IIR
            #self.__iir_filter()


    def __iir_filter(self):
        # Filter distance sensors
        for i in range(len(DIST_PINS)):
            self.dist_filt[i].pop() # dump oldest filtered val
            self.dist_filt[i].insert(0, 0) # insert filler to front
            for j in range(ORDER+1):
                self.dist_filt[i][0] = self.dist_raw[i][j]#*DIST_FF[j] - self.dist_filt[i][j]*DIST_FB[j]

        """
        # Filter magnetometer 
        self.mag_filt.pop() # dump oldest filtered val
        self.mag_filt.insert(0, 0) # insert filler to front
        for i in range(ORDER+1):
            self.mag_filt[0] += self.mag_raw[i]*MAG_FF[i] - self.mag_filt[i]*MAG_FB[i]
        """

    def getDistances(self):
        d = []
        for i in range(len(DIST_PINS)):
            d.append(self.dist_raw[i][0])
        return d

    
    def getOrientation(self):
        return self.mag_filt[0]

    """
    def __readADC0831(self, dist_pin, cs_pin):
        # Setup conversion
        t = .0001
        GPIO.output(cs_pin, 1)
        GPIO.output(CLK_PIN, 0)
        time.sleep(t)
        GPIO.output(cs_pin,0)
        time.sleep(t)
        # Read each bit
        temp_bits = []
        for i in range(8):
            GPIO.output(CLK_PIN, 1)
            time.sleep(t)
            GPIO.output(CLK_PIN, 0)
            time.sleep(t)
            temp_bits.append(GPIO.input(dist_pin))

        # Convert to int8
        temp = 0
        for i in range(8):
            temp += math.pow(2,i)*temp_bits.pop() # This should pop lsb to msb
        return temp
    """

    def __readPing(self, dist_pin):
        timeout = 0.020
        distance = 0
        avg = 5
        avg_value = 0
        counter = 0
        while counter < avg:
            GPIO.setup(dist_pin, GPIO.OUT)
            #clean up the output
            GPIO.output(dist_pin, 0)				
            #timing for ping
            time.sleep(0.000002)
            #send a signal
            GPIO.output(dist_pin, 1)
            #timing for ping
            time.sleep(0.00005)
            GPIO.output(dist_pin, 0)
            GPIO.setup(dist_pin, GPIO.IN)	
            reading = True
            begin = time.time()
            #start = time.time()
            while GPIO.input(dist_pin) == 0 and reading:
                start = time.time()
                if (start - begin > timeout):
                    reading = False

            if reading:
                #end = time.time()
                begin = time.time()
                while GPIO.input(dist_pin) == 1 and reading:
                    end = time.time()
                    if (end - begin > timeout):
                        reading = False
            if reading:
                duration = end - start
                #distance = duration*34000/2
                #return distance
                avg_value += duration
                counter += 1
            """
            while GPIO.input(dist_pin) == 0:
                starttime = time.time()

            while GPIO.input(dist_pin) == 1:
                endtime = time.time()
            duration = endtime-starttime
            avg_value += duration
            counter += 1
            """
        avg_value = avg_value/avg
        distance = avg_value*34000/2
        #distance = avg_value/avg
        if distance > 250:
            return 0
        else:
            return distance
            
            
    def __getOrientationLSM303(self):
        #accel, mag = self.lsm303.read()
        mag_x, mag_y, mag_z = 0, 0, 0 #mag
				# Get help here...
        # ALSO, need to filter to handle circular data
        angle = 0
        # Quadrant I
        if (mag_x > self.offset_x and mag_y > self.offset_y):
            angle = math.atan((mag_y-self.offset_y)/(mag_x-self.offset_x))
  
        #Quadrant II
        elif (mag_x < self.offset_x and mag_y > self.offset_y):
            new_x = -(mag_x-self.offset_x)
            angle = math.atan((mag_y-self.offset_y)/new_x)
            angle = math.pi - angle
  
        # Quadrant III
        elif (mag_x < self.offset_x and mag_y < self.offset_y):
            new_x = -(mag_x-self.offset_x)
            new_y = -(mag_y-self.offset_y)
            angle = math.atan(new_y/new_x)
            angle = math.pi + angle
            
        # Quadrant IV
        elif (mag_x > self.offset_x and mag_y < self.offset_y):
            angle = math.atan((mag_y-self.offset_y)/(mag_x-self.offset_x))
            angle = (2*math.pi) + angle
  
        return angle*180.0/math.pi
