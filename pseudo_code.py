# Pseudo code for partner-search algorithm

moves = [(0,1), (0,-1), (1, 0), (-1, 0)]

# LEADER ROBOT
stack = []
visited = []

stack.append((0,0))
curr_leader = (0,0)
while stack:
    explore = stack.pop()
    curr_leader = explore
    # *enter explore position*
    for i in range(0, 4):
        if IR_Sensor[i].clearPath() and (explore + moves[i]) not in visited:
            stack.append(explore + moves[i])

    # Position has now been expanded
    visited.append(explore) # Might want to store more info here (ie. open walls)

#====================================================================================================#

# FOLLOWER ROBOT 
stack_temp = []
visited_temp = []

stack_temp.append((0,0))
curr_follower = (0,0)
while stack_temp:
    explore = stack.pop()
    curr_follower = explore
    # *enter explore position*
    for i in range(0, 4):
        if IR_Sensor[i].clearPath() and (explore + moves[i]) not in visited_temp:
            stack_temp.append(explore + moves[i])

    # Position has now been expanded
    visited_temp.append(explore) # Might want to store more info here (ie. open walls)

    # After each node is expanded, will attempt to sync absolute position with master
    # Options include:
    # - incidental line of sight with other robot 
    # - location recognition by comparing images
    # - one robot waits until found by other (not ideal)

    if sync_succesful: # consolidate searches
        diff_origin = match_pos_leader - match_pos_follower
        # translate and empty temp list/stacks
        while stack_temp:
            stack.append(stack_temp.pop() + diff_origin)
        while visited_temp:
            visited.append(visited_temp.pop() + diff_origin)
        curr_follower += diff_origin

# Now follower will continue search using leaders information
while stack:
    explore = stack.pop()
    curr_follower = explore
    # *enter explore position*
    for i in range(0, 4):
        if IR_Sensor[i].clearPath() and (explore + moves[i]) not in visited:
            stack.append(explore + moves[i])

    # Position has now been expanded
    visited.append(explore) # Might want to store more info here (ie. open walls)
